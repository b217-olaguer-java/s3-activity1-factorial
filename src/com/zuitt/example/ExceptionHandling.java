package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {

    public static void main(String[] args) {
        // Exceptions
            // A problem that arises during the execution of a program
            // It disrupts the normal flow of the program and terminates it abnormally.

        // Exception Handling
            // Refers to managing and catching run-time errors in order to safely run our code.

        // Errors encountered in Java
            // Compile-time error -
            // Run-time error

        Scanner scInput = new Scanner(System.in);
        int num = 0;
        System.out.println("Please enter a number:");

        try { // try to execute the statement inside the try block
            num = scInput.nextInt();

        }
        catch (Exception e /* "e" is the identifier */) {
            System.out.println("Invalid Input");
            e.printStackTrace(); // this method prints the throwable error when the try code block is executed.
        }
        // finally is optional block.
        finally {
            System.out.println("You have entered: " + num);
        }
//        num = scInput.nextInt();
//        System.out.println("Hello come along with me.");


    }
}
