package com.zuitt.example;

import java.util.Scanner;

public class WDC043_S3_A1 {
    public static void main(String[] args) {

        Scanner scanInput = new Scanner(System.in);
        System.out.println("Please input an integer between 1 and 22 you would like to compute the Factorial of: ");

        long num;
        long fact = 1;
        long i = 1;

        // Factorial code
        try {
            num = scanInput.nextLong();
            if (num < 0 || num > 20) {
                System.out.println("Number Out-of-Range. Input only numbers between 0 and 21");
            } else if (num == 0) {
                System.out.println("The factorial of 0 is 1.");
            } else {
                while (i <= num) {
                    fact = fact * i;
                    i ++;
                }
                System.out.println("The Factorial of " + num + " is " + fact + ".");
            }
        } catch (Exception e) {
            System.out.println("Invalid input.");
            e.printStackTrace();
        }
        finally {
            System.out.println("Thank you for using this Java Factorial Counter. - AL -");
        }
    }
}
