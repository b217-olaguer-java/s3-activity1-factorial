package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepetitionControl {
    public static void main(String[] args) {
        // Loops
            // Control Structures that allow code blocks to be executed multiple times.

            // While Loop

//            int x = 0;
//            while(x < 10) {
//                System.out.println("Loop number: " + x);
//                x ++;
//            }

            // Do-While Loop
                // This is similar to while loop.
                // Executes code at least once.
                // READ ABOUT DO-WHILE LOOP -----------------------------------------------------
//            int y = 10;
//            do {
//                System.out.println("Countdown: " + y);
//                y --;
//            } while (y > 0);
//
//            // this prints "Countdown: 10"
//
//            // For Loop
//                // Syntax:
//                /* for (initialValue; condition; iteration) {
//                    Code block
//                    }
//                */
//            for (int i = 0; i < 10; i++) {
//                System.out.println("Current count: " + i);
//            }
//
//            // For Loop with arrays
//            int[] intArr = { 100, 200, 300, 400, 500 };
//
//            for (int i = 0; i < intArr.length; i++) {
//                System.out.println(intArr[i] + " " + "i-" + i);
//            }
//
///*
//            // for-each loop
//            // Syntax:
//                for (dayaType itemName: arrayName) {
//                    // Cocdeblock
//                }
//*/
//            String[] nameArray = {"John", "Paul", "George", "Ringo"};
//            for (String name: nameArray) {
//                System.out.println(name);
//            }
//
//            // Nested for Loops
//            String[][] classroom = new String[3][3];
//
//            // First Row
//            classroom[0][0] = "Athos";
//            classroom[0][1] = "Porthos";
//            classroom[0][2] = "Aramis";
//
//            // Second Row
//            classroom[1][0] = "Brandon";
//            classroom[1][1] = "Junjun";
//            classroom[1][2] = "Jobert";
//
//            // Third Row
//            classroom[2][0] = "Jolly";
//            classroom[2][1] = "Donald";
//            classroom[2][2] = "Grace";
//
//            for (int row = 0; row < 3; row ++) { // Outer loop
//                for (int col = 0; col < 3; col ++) { // inner loop
//                    System.out.println("Classroom [" + row + "]" + "[" + col + "] " + classroom[row][col]);
//                }
//            }
//
//            for (String[] row: classroom) {
//                for (String column: row) {
//                    System.out.println(column);
//                }
//            }

            // for-each with ArrayList
            /*
                arrayListName.forEach(consumer ->

            */

        ArrayList<Integer> numbers = new ArrayList<>();

        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        System.out.println("ArrayList" + numbers);

        // "->" Lambda Operator. - used to separate parameter and implementation.
        // "->" ~ single arrow
        numbers.forEach(number -> System.out.println("ArrayList: " + number));

        // forEach with HashMaps
        /*
            Syntax:
                hashMapName.forEach((key, value) -> code block);

        * */

        HashMap <String, Integer> grades = new HashMap<>(){
            {
                put("English", 90);
                put("Math", 95);
                put("Science", 97);
                put("History", 94);
            }
        };

        grades.forEach((subject /*these are parameters. 1st is KEY*/, grade /*This is the Value*/) -> System.out.println(subject + ": " + grade + "\n"));


    }

}
